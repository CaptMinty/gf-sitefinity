﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using Telerik.Web.UI;
using Telerik.Sitefinity.Security;

namespace SitefinityWebApp.Custom.CodeExplorer
{
    public partial class CodeEditor : System.Web.UI.Page
    {
        private string _filePath = null;
        private List<string> _allowPaths = null;
        private List<string> _allowExt = null;

        public string FilePath
        {
            get
            {
                if (_filePath == null)
                {
                    string path = Request.QueryString["file"];
                    if (!String.IsNullOrEmpty(path))
                    {
                        _filePath = HttpUtility.UrlDecode(path.TrimStart(new char[] { '/' }));
                    }
                }
                return _filePath;
            }
        }
  
        public List<string> AllowedPaths
        {
            get
            {
                if (_allowPaths == null)
                {
                    _allowPaths = new List<string>();
                    foreach (string item in FileManager.DEFAULT_ALLOWED_PATHS.Split(new char[] { ',' }))
                    {
                        _allowPaths.Add(this.MapPath(item));
                    }
                }
                return _allowPaths;
            }
        }

        public List<string> AllowedExt
        {
            get
            {
                if (_allowExt == null)
                {
                    _allowExt = new List<string>();
                    foreach (string item in FileManager.DEFAULT_ALLOWED_EXT.Split(new char[] { ',' }))
                    {
                        _allowExt.Add(item);
                    }
                }
                return _allowExt;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bool success = false;
                string response = this.ReadFile(this.FilePath, out success);
                txtContent.Text = response;
                
                //STORE ORIGINAL CONTENTS FOR HANDLING CONCURRENCY LATER
                Session["CodeExplorer.Contents"] = response;

                if (success)
                {
                    this.ApplyJS();
                }
                else
                {
                    RadAjaxManager1.Alert(response);
                }
                //WON'T NEED LOADING PANEL ANYMORE
                RadAjaxManager1.ResponseScripts.Add("setTimeout('showLoadingPanel(false)',12000);");
            }
        }

        protected void ApplyJS()
        {
            if (!String.IsNullOrEmpty(this.FilePath))
            {
                Page.Header.Controls.Add(new LiteralControl(String.Format(@"
                <script language=""javascript"" type=""text/javascript"" src=""{0}""></script>",
                    Page.ResolveUrl(FileManager.EDIT_AREA_PATH))));

                string ext = Path.GetExtension(this.FilePath).TrimStart(new char[] { '.' });
                string editAreaJS = String.Format(@"
                <script language=""javascript"" type=""text/javascript"">
                    editAreaLoader.init({{
	                    id : '{0}'		// textarea id
	                    ,syntax: '{1}'			// syntax to be uses for highgliting
	                    ,start_highlight: true		// to display with highlight mode on start-up
                        ,toolbar: 'save, |, undo, redo, |, search, go_to_line, |, word_wrap, highlight, change_smooth_selection, reset_highlight, syntax_selection, |, help'
                        ,save_callback: 'saveToDisk'
                        ,fullscreen: true
                        ,min_height: 300
                    }});
                </script>", txtContent.ClientID, this.GetSyntax(ext));
                Page.Header.Controls.Add(new LiteralControl(editAreaJS));
            }
        }

        protected string GetSyntax(string ext)
        {
            switch (ext.ToLower())
            {
                case "css":
                    return "css";
                case "js":
                    return "js";
                case "php":
                    return "php";
                case "cs":
                    return "cpp";
                case "vb":
                    return "vb";
                case "sql":
                    return "sql";
                case "xml":
                case "config":
                    return "xml";
                case "pl":
                    return "perl";
                case "java":
                    return "java";
                case "cf":
                    return "coldfusion";
                case "rb":
                    return "ruby";
                default:
                    return "html";
            }
        }

        protected string ReadFile(string path, out bool success)
        {
            //VALIDATE INPUT
            if (String.IsNullOrEmpty(path))
            {
                success = false;
                return "No file was provided.";
            }

            string fullpath = this.MapPath(path);
            if (File.Exists(fullpath))
            {
                if (this.IsApproved(fullpath))
                {
                    try
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(fullpath))
                        {
                            string contents = sr.ReadToEnd();
                            success = true;
                            return contents;
                        }
                    }
                    catch (Exception ex)
                    {
                        success = false;
                        return ex.Message;
                    }
                }
                else
                {
                    success = false;
                    return "Not an authorized file to modify!";
                }
            }
            else
            {
                success = false;
                return "The file does not exist!";
            }
        }

        protected bool SaveToFile(string path, out string msg)
        {
            return SaveToFile(path, txtContent.Text, out msg);
        }

        protected bool SaveToFile(string path, string contents, out string msg)
        {
            //VALIDATE INPUT
            if (String.IsNullOrEmpty(path))
            {
                msg = "No file was provided.";
                return false;
            }

            if (String.IsNullOrEmpty(contents))
            {
                msg = "The contents are empty. Please save at least one character (for safety).";
                return false;
            }

            string fullpath = this.MapPath(path);
            if (File.Exists(fullpath))
            {
                if (this.IsApproved(fullpath))
                {
                    //VERIFY FILE HAS NOT CHANGED BY ANOTHER USER (CONCURRENCY)
                    bool readSuccess;
                    string serverContents = this.ReadFile(path, out readSuccess);
                    if (readSuccess && serverContents.Equals(Session["CodeExplorer.Contents"]))
                    {
                        try
                        {
                            //Open file for writing and write content
                            using (StreamWriter externalFile = new StreamWriter(fullpath, false))
                            {
                                //SAVE CONTENTS TO ENSURE CONCURRENCY ON NEXT SAVE
                                Session["CodeExplorer.Contents"] = contents;

                                externalFile.Write(contents);
                                msg = "File was successfully save.";
                                return true;
                            }
                        }
                        catch (Exception ex)
                        {
                            msg = ex.Message;
                            return false;
                        }
                    }
                    else
                    {
                        msg = "File has been changed by another user! Please reopen the file and add your changes.";
                        return false;
                    }
                }
                else
                {
                    msg = "Not an authorized file!";
                    return false;
                }
            }
            else
            {
                msg = "The file does not exist!";
                return false;
            }
        }

        protected bool IsApproved(string path)
        {
            //VALIDATE INPUT
            if (String.IsNullOrEmpty(path))
                return false;

            return SecurityManager.IsBackendUser() && this.AllowedPaths.Exists(item => path.StartsWith(item, StringComparison.OrdinalIgnoreCase)) &&
                this.AllowedExt.Exists(item => path.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            string msg = String.Empty;
            if (this.SaveToFile(this.FilePath, e.Argument, out msg))
            {
                string js = String.Format(@"
                document.getElementById('lastSaved').style.display = 'block';
                document.getElementById('lastSaved').innerText='({0} saved {1})';",
                        Path.GetFileName(this.FilePath), DateTime.Now.ToLongTimeString());
                RadAjaxManager1.ResponseScripts.Add(js);
            }
            RadAjaxManager1.Alert(msg);
        }
    }
}