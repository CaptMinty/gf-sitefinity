﻿using Babaganoush.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Sitefinity.Pages.Model;

namespace SitefinityWebApp.Mvc.Models
{
	public class ResponsiveNavigationModel
	{
		public PageModel NavigationPages { get; set; }
		public PageNode CurrentPage { get; set; }
	}
}
