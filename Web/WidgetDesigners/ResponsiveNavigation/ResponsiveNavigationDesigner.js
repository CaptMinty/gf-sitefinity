Type.registerNamespace("SitefinityWebApp.WidgetDesigners.ResponsiveNavigation");

SitefinityWebApp.WidgetDesigners.ResponsiveNavigation.ResponsiveNavigationDesigner = function (element) {
    /* Initialize ControlMode fields */
    this._controlMode = null;
    
    /* Calls the base constructor */
    SitefinityWebApp.WidgetDesigners.ResponsiveNavigation.ResponsiveNavigationDesigner.initializeBase(this, [element]);
}

SitefinityWebApp.WidgetDesigners.ResponsiveNavigation.ResponsiveNavigationDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        SitefinityWebApp.WidgetDesigners.ResponsiveNavigation.ResponsiveNavigationDesigner.callBaseMethod(this, 'initialize');
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        SitefinityWebApp.WidgetDesigners.ResponsiveNavigation.ResponsiveNavigationDesigner.callBaseMethod(this, 'dispose');
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control().Settings; /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI ControlMode */
        jQuery(this.get_controlMode()).val(controlData.ControlMode);
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control().Settings;

        /* ApplyChanges ControlMode */
        controlData.ControlMode = jQuery(this.get_controlMode()).val();
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

    /* --------------------------------- properties -------------------------------------- */

    /* ControlMode properties */
    get_controlMode: function () { return this._controlMode; }, 
    set_controlMode: function (value) { this._controlMode = value; }
}

SitefinityWebApp.WidgetDesigners.ResponsiveNavigation.ResponsiveNavigationDesigner.registerClass('SitefinityWebApp.WidgetDesigners.ResponsiveNavigation.ResponsiveNavigationDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
