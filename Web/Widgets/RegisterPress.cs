﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Net.Mail;
using System.Web.UI;
using Telerik.Sitefinity.Security.Web.UI;
using Telerik.Sitefinity.Security;
using System.Web.Configuration;
using Telerik.Sitefinity.Security.Model;
using Telerik.Sitefinity.Security.Claims;
using Telerik.Sitefinity.Web.UI;

namespace SitefinityWebApp.Widgets
{
    public class RegisterPress : RegistrationForm
    {
        public override string LayoutTemplatePath
        {
            get
            {
                return layoutTemplatePath;
            }
        }

        protected override void ConfirmRegistration(UserManager userManager, Telerik.Sitefinity.Security.Model.User user)
        {
            base.ConfirmRegistration(userManager, user);
            UserProfileManager manager = UserProfileManager.GetManager();
            UserProfile profile = manager.GetUserProfile(user, typeof(SitefinityProfile).FullName);
            var firstName = Telerik.Sitefinity.Model.DataExtensions.GetValue(profile, "FirstName");
            var lastName = Telerik.Sitefinity.Model.DataExtensions.GetValue(profile, "LastName");
            var companyName = Telerik.Sitefinity.Model.DataExtensions.GetValue(profile, "CompanyName");
            // code to send form email
            string userName = user.UserName;
            string email = user.Email;
            string fName = firstName.ToString();
            string lName = lastName.ToString();
            string cName = companyName.ToString();
            string url = "video-broadcast-assets";

            SendEmail(userName, email, fName, lName, cName);
            var logIn = SecurityManager.AuthenticateUser("Default", userName, true, out user);

            // Reload page
            if(HttpContext.Current.Request.QueryString["ReturnUrl"] != null)
                url = HttpContext.Current.Request.QueryString["ReturnUrl"];

            HttpContext.Current.Response.Redirect(url);

        }

        protected void SendEmail(string userName, string email, string firstName, string lastName, string cName)
        {
            MailMessage mail = new MailMessage();
            string from = "noreply@globalfoundries.com";
            string subject = "New User for Press Page";
            SmtpClient client = new SmtpClient(APPGlobalSetting.SMTP);
            string toEmails = APPGlobalSetting.ToEmails;
            StringBuilder content = new StringBuilder();
            content.Append("The following user registered with Global Foundries.");
            content.Append(Environment.NewLine);
            content.Append("First Name: ");
            content.Append(firstName);
            content.Append(Environment.NewLine);
            content.Append("Last Name: ");
            content.Append(lastName);
            content.Append(Environment.NewLine);
            content.Append("Company Name: ");
            content.Append(cName);
            content.Append(Environment.NewLine);
            content.Append("Username: ");
            content.Append(userName);
            content.Append(Environment.NewLine);
            content.Append("Email: ");
            content.Append(email);

            try
            {
                
                mail.From = new MailAddress(from, from);
                mail.Subject = subject;
                mail.Body = content.ToString();
                
                string[] toAddress = toEmails.Split(';');
                foreach (string address in toAddress)
                {
                    mail.To.Add(new MailAddress(address, address));
                }

                client.Send(mail);
                

            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Message: {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);
            }
        }

        private const string layoutTemplatePath = "~/Widgets/RegisterPress.ascx";
    }

    public static class APPGlobalSetting
    {
        ///<summary>
        /// Configuration settings for sending emails when a user registers
        ///</summary>

        static public string SMTP { get; set; }
        static public string ToEmails { get; set; }

        static APPGlobalSetting()
        {
            SMTP = WebConfigurationManager.AppSettings["SMTP"];
            ToEmails = WebConfigurationManager.AppSettings["ToEmails"];

        }
    }
}