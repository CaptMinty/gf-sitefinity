﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Model.ContentLinks;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SitefinityWebApp.Widgets
{
    public class VideoDetails
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string DownloadUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string FileSize { get; set; }
    }
    public partial class VideoDownloads : System.Web.UI.UserControl
    {
        private string vidPath = HttpContext.Current.Request.PhysicalApplicationPath;

        protected void Page_Load(object sender, EventArgs e)
        {
           
            List<DynamicContent> items = GetDynamicItems().ToList();
            var list = GetVideos(items);
            vidRepeater.DataSource = list;
            vidRepeater.DataBind();
        }

        protected List<VideoDetails> GetVideos(List<DynamicContent> items)
        {
            
           
            List<VideoDetails> videos = new List<VideoDetails>();
           
            foreach (var item in items) 
            {
                VideoDetails details = new VideoDetails();
                details.Title = item.GetValue("Title").ToString();
                details.DownloadUrl = item.GetValue("Media_Url").ToString();
                details.Description = item.GetValue("Description").ToString();
                details.FileSize = item.GetValue("File_Size").ToString();
                SetImageUrl(item, details);
                videos.Add(details);
               
            }


            return videos;
        }

         public void SetImageUrl(DynamicContent item, VideoDetails detail)
        {
            var mainImageLinks = item.GetValue("Thumbnail_Url") as ContentLink[];
            if (mainImageLinks != null && mainImageLinks.Any())
            {
                var mainImageLink = mainImageLinks[0];
                if (mainImageLink != null)
                {
                    Guid imageId = mainImageLink.ChildItemId;
                    Telerik.Sitefinity.Libraries.Model.Image mainImage = null;
                    try
                    {
                        mainImage = LibrariesManager.GetManager().GetImage(imageId);
                    }
                    catch (Exception)
                    {
                        detail.ThumbnailUrl = "#";
                    }
                    if (mainImage != null)
                    {
                        detail.ThumbnailUrl = mainImage.Url;
                    }
                }
             }
          }

        protected void vidRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                string fileName = e.CommandArgument.ToString();
                string path = vidPath + fileName;
                byte[] bts = File.ReadAllBytes(path);
                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", bts.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachement; filename=" + fileName);
                Response.BinaryWrite(bts);
                Response.Flush();
                Response.End();
            }
        }

        public IQueryable<DynamicContent> GetDynamicItems()
        {
            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager();
            Type pressMediaItemType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.PressMediaItems.PressMediaItem");

            var mediaItems = dynamicModuleManager.GetDataItems(pressMediaItemType).Where(cit => cit.Status == ContentLifecycleStatus.Live && cit.Visible);

            return mediaItems;
        }
    }
}