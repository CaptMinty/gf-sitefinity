===================================================
Name: Falafel WebApi for Sitefinity
Written by: Falafel Software Inc.
Website: http://www.falafel.com
===================================================

Thank you for downloading Falafel WebApi for Sitefinity!
Below is a quick overview to get you started.

------------------------------------------
Running Environment:
------------------------------------------
The Falafel WebApi for Sitefinity has been fully tested on
  -Sitefinity Version 5+
  -IIS7+

------------------------------------------
Installation:
------------------------------------------
1) The following app settings have been added to your web.config file:
    <add key="Falafel.WebApi.Enable" value="true" />
    <add key="Falafel.Mvc.EnableClassicRoutes" value="false" />
2) Setting "Falafel.WebApi.Enable" to true will add routes for web services under "~/api"
2) Setting "Falafel.Mvc.EnableClassicRoutes" to true will add routes for classic MVC under "~/mvcroute"
	Note: Useful for generating client-side views on the server-side for more processing and security.

Note: By default, Sitefinity does not enforce permissions on its API for performance.
	If you need to enforce permissions on your web service, follow this knowledgebase:
	http://www.sitefinity.com/developer-network/knowledge-base/changing-filtering-by-view-permissions-in-5-0

------------------------------------------
Usage:
------------------------------------------
1) A vast collection of Web API services are included. For a JavaScript wrapper, see here:
	https://github.com/FalafelSoftwareInc/Falafel.Async/blob/master/Async/libs/sitefinity/js/api.js

2)  You can override and extend the web services in the "~/Api" folder

------------------------------------------
Sitefinity Training:
------------------------------------------
We offer step by step instructor-led training online and onsite to train
your team on using, configuring , optimizing and developing for Sitefinity 5. For
classes and schedule, please visit: http://www.falafel.com/consulting/sitefinity-services

------------------------------------------
Custom Sitefinity Solutions:
------------------------------------------
Falafel can make your Sitefinity experience perfect. 
Whether you need help installing Sitefinity for the first time, 
integrating with another system or a custom module, Falafel can help. 
Contact us at info@falafel.com to discuss your project.


ENJOY!!